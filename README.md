nsa-desktop
===========

Desktop version of NSA
Provider of the custom made TerminalFactory for PC/SC with NFC at a mobile device

How to build the project
------------------------

The project is built with ant, it uses log4j for logging and [RXTXcomm](http://rxtx.qbang.org/wiki/index.php/Main_Page) for the communication via USB.
You have to install the RXTX System Libaries into your Java SDK to make RXTX work.

Additional Linux Tasks
----------------------------------------
For Linux you have to enable `/dev/ttyUSB0` to connect to "Nokia 6210 classic" via USB

	
How to develop the project
------------------------
You have to install the RXTX System Libaries into your Java SDK to make RXTX work.
