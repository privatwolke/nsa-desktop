package at.ac.tuwien.mnsa.smartcard;

public class MNSAConstants {

	final static String PROVIDER_NAME = "MNSAProvider";
	final static double PROVIDER_VERSION = 0.1;
	final static String PROVIDER_INFO = "MNSAProvider Implementation";
	
	final static String TERMINAL_ENGINE = "TerminalFactory.PC/SC";
	
}
