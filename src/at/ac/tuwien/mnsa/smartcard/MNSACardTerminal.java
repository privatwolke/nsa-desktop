package at.ac.tuwien.mnsa.smartcard;

import java.io.IOException;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;

import at.ac.tuwien.mnsa.connection.Connection;
import at.ac.tuwien.mnsa.protocol.Message;
import at.ac.tuwien.mnsa.protocol.ProtocolException;
import at.ac.tuwien.mnsa.protocol.ResetReceivedException;
import at.ac.tuwien.mnsa.protocol.Result;

public class MNSACardTerminal extends CardTerminal {

	private static int POLLING_INTERVAL = 100;
	private Connection connection;

	private MNSACardTerminal() {}

	public MNSACardTerminal(Connection connection) {
		this();
		this.connection = connection;
	}

	/**
	 * Success iff USB conncetion open + card connected
	 */
	@Override
	public Card connect(String protocol) throws CardException {
		if (!this.isCardPresent())
			throw new CardException("No card present");
		return new MNSAProxyCard(connection);
	}

	@Override
	public String getName() {
		return MNSAConstants.TERMINAL_ENGINE;
	}

	@Override
	public boolean isCardPresent() throws CardException {
		try {
			Result result = connection.transmit(Message.IS_CARD_PRESENT);
			connection.disconnect();
			return result.isPositive();
		} catch (IOException | ProtocolException | ResetReceivedException e) {
			throw new CardException(e);
		}
	}

	@Override
	public boolean waitForCardAbsent(long timeout) throws CardException {
		long sleeptime = timeout / POLLING_INTERVAL;
		for (short i = 0; i < POLLING_INTERVAL; i++) {
			if (!isCardPresent())
				return true;
			try {
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				break;
			}
		}
		return false;
	}

	@Override
	public boolean waitForCardPresent(long timeout) throws CardException {
		long sleeptime = timeout / POLLING_INTERVAL;
		for (short i = 0; i < POLLING_INTERVAL; i++) {
			if (isCardPresent())
				return true;
			try {
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				break;
			}
		}
		return false;
	}

}
