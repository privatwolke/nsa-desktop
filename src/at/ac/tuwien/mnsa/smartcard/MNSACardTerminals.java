package at.ac.tuwien.mnsa.smartcard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;

import at.ac.tuwien.mnsa.connection.Connection;

public class MNSACardTerminals extends CardTerminals {
	
	private ConcurrentHashMap<String, MNSACardTerminal> terminals;
	
	private MNSACardTerminals() {}

	public MNSACardTerminals(Connection connection) {
		this();
		this.terminals = new ConcurrentHashMap<String, MNSACardTerminal>();
		this.terminals.put("nsa-desktop-reader", new MNSACardTerminal(connection));
  }

	@Override
  public List<CardTerminal> list(State state) throws CardException {
	  List<CardTerminal> result = new ArrayList<CardTerminal>();
	  
	  for (Entry<String, MNSACardTerminal> c : terminals.entrySet()) {
	  	result.add(c.getValue());
	  }
	  
	  return result;
  }

	@Override
  public boolean waitForChange(long timeout) throws CardException {
	  return false;
  }

}
