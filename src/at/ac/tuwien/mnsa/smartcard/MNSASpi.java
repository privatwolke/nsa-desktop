package at.ac.tuwien.mnsa.smartcard;

import java.io.IOException;

import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;

import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactorySpi;

import at.ac.tuwien.mnsa.connection.Connection;
import at.ac.tuwien.mnsa.connection.USBConnectionFactory;

public class MNSASpi extends TerminalFactorySpi {
	
	private MNSACardTerminals terminals;
	
	public MNSASpi(Object param) {
		try {
			Connection c = USBConnectionFactory.getConnection();
			if(c == null){
				throw new IOException("No CardReader found.");
			}
			this.terminals = new MNSACardTerminals(c);
		} catch (PortInUseException | UnsupportedCommOperationException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
  protected CardTerminals engineTerminals() {
	  return this.terminals;
  }

}
