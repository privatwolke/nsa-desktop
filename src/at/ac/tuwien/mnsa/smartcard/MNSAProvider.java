package at.ac.tuwien.mnsa.smartcard;

import java.security.Provider;

public class MNSAProvider extends Provider {

  private static final long serialVersionUID = 1L;

	public MNSAProvider() {
	  super(MNSAConstants.PROVIDER_NAME, MNSAConstants.PROVIDER_VERSION, MNSAConstants.PROVIDER_INFO);
	  put(MNSAConstants.TERMINAL_ENGINE, "at.ac.tuwien.mnsa.smartcard.MNSASpi");
  }
	
}
