package at.ac.tuwien.mnsa.smartcard;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;

import at.ac.tuwien.mnsa.connection.Connection;
import at.ac.tuwien.mnsa.protocol.Message;
import at.ac.tuwien.mnsa.protocol.ProtocolException;
import at.ac.tuwien.mnsa.protocol.ResetReceivedException;
import at.ac.tuwien.mnsa.protocol.Message.ActionIdentifiers;

/**
 * Provides a Proxy for at.ac.tuwien.mnsa.mobile.CardSkeleton.
 */
public class MNSAProxyCard extends Card {

	private class MNSAProxyCardChannel extends CardChannel {

		@Override
		public void close() throws CardException {}

		@Override
		public Card getCard() {
			return MNSAProxyCard.this;
		}

		@Override
		public int getChannelNumber() {
			try {
				ByteBuffer buffer = ByteBuffer.wrap(connection.transmit(Message.GET_CHANNEL_NUMBER).getPayload());
				buffer.order(ByteOrder.LITTLE_ENDIAN);
				return (int) buffer.getShort();
			} catch (IOException | ProtocolException | ResetReceivedException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public ResponseAPDU transmit(CommandAPDU command) throws CardException {
			try {
				return new ResponseAPDU(connection.transmit(
					new Message(ActionIdentifiers.TRANSMIT, command.getBytes())).getPayload());
			} catch (IOException | ProtocolException | ResetReceivedException e) {
				throw new CardException(e);
			}
		}

		@Override
		public int transmit(ByteBuffer command, ByteBuffer response) throws CardException {
			try {
				byte[] buffer = connection.transmit(
					new Message(ActionIdentifiers.TRANSMIT, command.array())).getPayload();
				response.put(buffer);
				return buffer.length;
			} catch (IOException | ProtocolException | ResetReceivedException e) {
				throw new CardException(e);
			}
		}

	}

	/**
	 * Holds the Connection to the Reader.
	 */
	private Connection connection;

	/**
	 * Constructs a new Proxy.
	 */
	public MNSAProxyCard(Connection connection) {
		this.connection = connection;
	}

	@Override
	public void beginExclusive() throws CardException {}

	@Override
	public void disconnect(boolean reset) throws CardException {
		connection.disconnect();
	}

	@Override
	public void endExclusive() throws CardException {}

	@Override
	public ATR getATR() {
		try {
			return new ATR(connection.transmit(Message.GET_ATR).getPayload());
		} catch (IOException | ProtocolException | ResetReceivedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public CardChannel getBasicChannel() {
		return new MNSAProxyCardChannel();
	}

	@Override
	public String getProtocol() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CardChannel openLogicalChannel() throws CardException {
		return new MNSAProxyCardChannel();
	}

	@Override
	public byte[] transmitControlCommand(int arg0, byte[] arg1) throws CardException {
		return null;
	}

}
